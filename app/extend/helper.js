'use strict';

module.exports = {
  md5(str) {
    const utility = require('utility');// 密码加密
    const secretStr = this.app.config.account.secretStr;// 引入config.default.js中配置的加密字串
    if (!str) return '';
    return utility.md5(str + secretStr);
  },
//   removeArryEnd(str, char) {
//     const t_arry = str.split(char);
//     t_arry.pop();
//     return t_arry.join(char);
//   },
//   getUserId() {
//     const { app, ctx } = this;
//     const secret = app.config.jwt.secret;
//     const passportType = app.config.auth.passport.type;

//     if (passportType === 'jwt') {
//       const token = ctx.request.header.authorization || ctx.cookies.get('authorization');
//       const decode = app.jwt.verify(token, secret);
//       if (decode && decode.userId) return decode.userId;
//     } else if (passportType === 'session') {
//       return this.ctx.session.userId;
//     }
//   },
//   getUserName() {
//     const { app, ctx } = this;
//     const secret = app.config.jwt.secret;
//     const passportType = app.config.auth.passport.type;

//     if (passportType === 'jwt') {
//       const token = ctx.request.header.authorization || ctx.cookies.get('authorization');
//       const decode = app.jwt.verify(token, secret);
//       if (decode && decode.userName) return decode.userName;
//     } else if (passportType === 'session') {
//       return this.ctx.session.userName;
//     }
//   },

  randomNo(j) {
    // j位随机数，用以加在时间戳后面。
    let random_no = '';
    for (let i = 0; i < j; i++) {
      random_no += Math.floor(Math.random() * 10);
    }
    random_no = new Date().getTime() + random_no;
    return random_no;
  },
};