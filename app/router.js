'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  const auth = app.middleware.auth();

  router.get('/', auth, controller.home.index);
  router.get('/login', controller.home.login);
  router.get('/getcode', controller.home.getCode);

  router.post('/userInfo/check', controller.userInfo.check);
  router.get('/userLogout', auth, controller.home.userLogout);

  router.get('/console/console1', auth, controller.console.console.console1);
  router.get('/console/console2', auth, controller.console.console.console2);


  router.get('/masterData/goods', auth, controller.masterData.goods.index);
  router.get('/masterData/goods/find', auth, controller.masterData.goods.find);
  router.post('/masterData/goods/add', auth, controller.masterData.goods.add);
  router.post('/masterData/goods/update', auth, controller.masterData.goods.update);
  router.post('/masterData/goods/del', auth, controller.masterData.goods.del);

  router.get('/masterData/goodType', auth, controller.masterData.goodType.index);
  router.get('/masterData/goodType/find', auth, controller.masterData.goodType.find);
  router.post('/masterData/goodType/add', auth, controller.masterData.goodType.add);
  router.post('/masterData/goodType/update', auth, controller.masterData.goodType.update);
  router.del('/masterData/goodType/:id', auth, controller.masterData.goodType.del);
  router.del('/masterData/goodType/batchRemove/:id', auth, controller.masterData.goodType.batchRemove)

  router.get('/masterData/goodUnit', auth, controller.masterData.goodUnit.index);
  router.get('/masterData/goodUnit/find', auth, controller.masterData.goodUnit.find);
  router.post('/masterData/goodUnit/add', auth, controller.masterData.goodUnit.add);
  router.post('/masterData/goodUnit/update', auth, controller.masterData.goodUnit.update);
  router.del('/masterData/goodUnit/:id', auth, controller.masterData.goodUnit.del);
  router.del('/masterData/goodUnit/batchRemove/:id', auth, controller.masterData.goodUnit.batchRemove)

  router.get('/masterData/warehouse', auth, controller.masterData.warehouse.index);
  router.get('/masterData/warehouse/find', auth, controller.masterData.warehouse.find);
  router.post('/masterData/warehouse/add', auth, controller.masterData.warehouse.add);
  router.post('/masterData/warehouse/update', auth, controller.masterData.warehouse.update);
  router.del('/masterData/warehouse/:id', auth, controller.masterData.warehouse.del);
  router.del('/masterData/warehouse/batchRemove/:id', auth, controller.masterData.warehouse.batchRemove)

  router.get('/masterData/position', auth, controller.masterData.position.index);
  router.get('/masterData/position/find', auth, controller.masterData.position.find);
  router.post('/masterData/position/add', auth, controller.masterData.position.add);
  router.post('/masterData/position/update', auth, controller.masterData.position.update);
  router.del('/masterData/position/:id', auth, controller.masterData.position.del);
  router.del('/masterData/position/batchRemove/:id', auth, controller.masterData.position.batchRemove)
  
  router.get('/masterData/customerType', auth, controller.masterData.customerType.index);
  router.get('/masterData/customerType/find', auth, controller.masterData.customerType.find);
  router.post('/masterData/customerType/add', auth, controller.masterData.customerType.add);
  router.post('/masterData/customerType/update', auth, controller.masterData.customerType.update);
  router.del('/masterData/customerType/:id', auth, controller.masterData.customerType.del);
  router.del('/masterData/customerType/batchRemove/:id', auth, controller.masterData.customerType.batchRemove)

  router.get('/masterData/vendorType', auth, controller.masterData.vendorType.index);
  router.get('/masterData/vendorType/find', auth, controller.masterData.vendorType.find);
  router.post('/masterData/vendorType/add', auth, controller.masterData.vendorType.add);
  router.post('/masterData/vendorType/update', auth, controller.masterData.vendorType.update);
  router.del('/masterData/vendorType/:id', auth, controller.masterData.vendorType.del);
  router.del('/masterData/vendorType/batchRemove/:id', auth, controller.masterData.vendorType.batchRemove)
  
  router.get('/masterData/vendorInfo', auth, controller.masterData.vendorInfo.index);
  router.get('/masterData/vendorInfo/find', auth, controller.masterData.vendorInfo.find);
  router.post('/masterData/vendorInfo/add', auth, controller.masterData.vendorInfo.add);
  router.post('/masterData/vendorInfo/update', auth, controller.masterData.vendorInfo.update);
  router.del('/masterData/vendorInfo/:id', auth, controller.masterData.vendorInfo.del);
  router.del('/masterData/vendorInfo/batchRemove/:id', auth, controller.masterData.vendorInfo.batchRemove)
};
