'use strict';

const { Controller } = require('egg');

class ConsoleController extends Controller {
  async console1() {
    const { ctx } = this;
  //  ctx.body = 'hi, egg';
    let dataList = {};
    await this.ctx.render('console/console1.njk', dataList);
  }

  async console2() {
    const { ctx } = this;
  //  ctx.body = 'hi, egg';
    let dataList = {};
    await this.ctx.render('console/console2.njk', dataList);
  }
}

module.exports = ConsoleController;
