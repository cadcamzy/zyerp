'use strict';

const { Controller } = require('egg');

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
  //  ctx.body = 'hi, egg';
    let dataList = {};
    await this.ctx.render('index.njk', dataList);
  }

  async login() {
    const { ctx } = this;
    let dataList = {};
    await this.ctx.render('login.njk', dataList);
  }

  async getCode() {
    const options = {
      width: 114,
      height: 42, // height of captcha
      fontSize: 50, // captcha text size
      color: true,
      ignoreChars: '0oO1ilI',
      noise: 4,
    };
    const svgcaptcha = require('svg-captcha');
    // 生成验证码，并写入session
    const captcha = svgcaptcha.createMathExpr(options);
    // 保存到session
    this.ctx.session.login_code = captcha.text;
    this.ctx.session.maxAge = 1000 * 60 * 10;
    this.ctx.type = 'svg';
    this.ctx.body = captcha.data;
  }

  async userLogout() {
    const { ctx, config } = this;

    ctx.session.name = null;
    ctx.session.userId = null;
    ctx.session.loginTime = null;

    ctx.redirect('/login');
  }
}

module.exports = HomeController;
