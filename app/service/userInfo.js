// app/service/user.js
const Service = require('egg').Service;

class UserInfoService extends Service {
  async check(userInfo) {
    const { username, password } = userInfo;
    const user = await this.app.mysql.get('user_info', { username, password });
    delete user.password;

    return { ...user };

  }

  async find(userInfo) {
    const { id } = userInfo;
    const user = await this.app.mysql.get('user_info', { id });

    delete user.password;

    return { ...user };
  }
}

module.exports = UserInfoService;