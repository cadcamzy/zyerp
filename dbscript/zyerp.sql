CREATE TABLE `zyerp`.`user_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `salt` VARCHAR(255) NULL,
  `status` INT NULL,
  `realName` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `avatar` VARCHAR(45) NULL,
  `sex` INT NULL,
  `phone` VARCHAR(20) NULL,
  `enable` INT NULL,
  `login` INT NULL,
  `roleIds` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COMMENT = '用户信息表';

INSERT INTO `zyerp`.`user_info` (`username`, `password`, `status`, `realName`, `email`, `sex`, `enable`) VALUES ('admin', 'B381165F6F212F2FF36B67F6AB2FB4AC', '1', '管理员', 'cadcamzy@Hotmail.com', '1', '1');


CREATE TABLE `zyerp`.`goods` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(45) NULL COMMENT '\'物料编码\'',
  `type` VARCHAR(45) NULL COMMENT '\'物料类别\'',
  `pic` VARCHAR(45) NULL COMMENT '\'图片\'',
  `name` VARCHAR(45) NULL COMMENT '\'名称\'',
  `model` VARCHAR(45) NULL COMMENT '\'型号\'',
  `bar_code` VARCHAR(45) NULL COMMENT '\'条码\'',
  `unit` VARCHAR(45) NULL COMMENT '\'单位\'',
  `is_batch` VARCHAR(1) NULL COMMENT '\'是否批次管理\'',
  `is_mwa` VARCHAR(1) NULL COMMENT '\'计价方法\'',
  `price` DECIMAL(12,3) NULL DEFAULT 1 COMMENT '标准价',
  `comments` VARCHAR(250) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) VISIBLE)
COMMENT = '物料基础表';

ALTER TABLE `zyerp`.`goods` 
ADD COLUMN `buy_unit` VARCHAR(45) NULL COMMENT '采购单位' AFTER `unit`,
ADD COLUMN `enable` INT NULL DEFAULT 1 AFTER `comments`,
ADD COLUMN `currency` VARCHAR(45) NULL DEFAULT 'RMB' COMMENT '货币单位' AFTER `enable`,
CHANGE COLUMN `is_mwa` `is_mwa` VARCHAR(1) NOT NULL COMMENT '\'计价方法\'\n0: 标准价\n1: 移动加权平均' ,
CHANGE COLUMN `type` `type` VARCHAR(45) NOT NULL COMMENT '\'物料类别\'' ,
CHANGE COLUMN `name` `name` VARCHAR(45) NOT NULL COMMENT '\'名称\'' ,
CHANGE COLUMN `unit` `unit` VARCHAR(45) NOT NULL COMMENT '\'单位\'' ,
CHANGE COLUMN `is_batch` `is_batch` VARCHAR(1) NOT NULL COMMENT '\'是否批次管理\'' ;

CREATE TABLE `zyerp`.`good_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `enable` INT NOT NULL DEFAULT 1 COMMENT '0:禁用 1:启用',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
COMMENT = '物料类型';

CREATE TABLE `zyerp`.`good_unit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `unit` VARCHAR(45) NULL COMMENT '单位',
  `enable` INT NULL DEFAULT 1 COMMENT '0:禁用；1:启用',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `unit_UNIQUE` (`unit` ASC) VISIBLE)
COMMENT = '计量单位表';

CREATE TABLE `zyerp`.`warehouse` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL COMMENT '名称',
  `owner` VARCHAR(45) NULL COMMENT '责任人',
  `enable` INT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
COMMENT = '仓库信息';

CREATE TABLE `zyerp`.`position` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `w_id` INT NOT NULL COMMENT '所属仓库ID',
  `code` VARCHAR(45) NOT NULL COMMENT '编码',
  `bar_code` VARCHAR(45) NULL COMMENT '条码',
  `name` VARCHAR(45) NOT NULL COMMENT '名称',
  `volume` DECIMAL(12,2) NULL COMMENT '体积',
  `volume_unit` VARCHAR(45) NULL COMMENT '体积单位',
  `weight` DECIMAL(12,2) NULL COMMENT '重量',
  `weight_unit` VARCHAR(45) NULL COMMENT '重量单位',
  `enable` INT NULL DEFAULT 1 COMMENT '启用',
  `comments` VARCHAR(250) NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) VISIBLE)
COMMENT = '仓位信息';

CREATE TABLE `zyerp`.`customer_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `enable` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
COMMENT = '客户类型';

CREATE TABLE `zyerp`.`vendor_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `enable` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
COMMENT = '供应商类别';

CREATE TABLE `zyerp`.`vendor_info` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` INT NOT NULL COMMENT '供应商类型',
  `code` VARCHAR(45) NOT NULL COMMENT '供应商编码',
  `name` VARCHAR(45) NULL,
  `address` VARCHAR(255) NULL,
  `contacts` VARCHAR(45) NULL COMMENT '联系人',
  `tel` VARCHAR(45) NULL,
  `tax_type` INT NOT NULL DEFAULT 0 COMMENT '发票类型： 0：普票； 1：专票',
  `tax_name` VARCHAR(45) NULL COMMENT '公司全称',
  `tax_code` VARCHAR(45) NULL COMMENT '纳税人识别号',
  `tax_bank` VARCHAR(45) NULL COMMENT '开户行',
  `tax_account` VARCHAR(45) NULL COMMENT '账号',
  `tax_address` VARCHAR(45) NULL COMMENT '开票地址',
  `tax_tel` VARCHAR(45) NULL COMMENT '开票电话',
  `comments` VARCHAR(250) NULL COMMENT '备注',
  `enable` INT NULL DEFAULT 1 COMMENT '启用',

  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE,
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) VISIBLE,
  PRIMARY KEY (`id`))
COMMENT = '供应商信息表';



