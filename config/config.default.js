/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1684841686489_8824';

  config.view = {
    defaultViewEngine: 'nunjucks',
    mapping: {
      '.tpl': 'nunjucks',
      '.njk': 'nunjucks'
    }
  }

  config.mysql = {
    client: {
      host: '127.0.0.1',
      port: '3306',
      user: 'root',
      password: '800116',
      database: 'zyerp',
      debug: false
    },
    app: true,
    agent: false,
  }

  config.account = {
    secretStr: 'sdf123@^%3891'
  }

  // add your middleware config here
  config.middleware = [];

  // add your user config here
  const userConfig = {
    defaultMaxRows: 500, //默认返回数据条件上限值 
    // myAppName: 'egg',
  };

  return {
    ...config,
    ...userConfig,
  };
};
